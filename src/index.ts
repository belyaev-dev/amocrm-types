import {NewLead, CreatedLead, Lead} from './types/lead';
import {
  NewContact,
  CreatedContact,
  Contact,
  ContactQuery,
} from './types/contact';
import {
  EmailFieldEnumCodes,
  PhoneFieldEnumCodes,
} from './types/custom_fields/multitext';
import {Tag, TagBase} from './types/tag';
import {
  UnsortedChatClient,
  UnsortedChatOrigin,
  UnsortedChatMetadata,
  UnsortedFormsMetadata,
  UnsortedSipMetadata,
  UnsortedTypes,
  UnsortedMailMetadata,
  UnsortedMailOrigin,
} from './types/unsorted';

export {
  // Сделка
  NewLead,
  CreatedLead,
  Lead,
  // Контакты
  NewContact,
  CreatedContact,
  Contact,
  ContactQuery,
  // Multitext Field Enum
  EmailFieldEnumCodes,
  PhoneFieldEnumCodes,
  // Тег
  TagBase,
  Tag,
  // Неразобранное
  UnsortedChatOrigin,
  UnsortedChatClient,
  UnsortedMailOrigin,
  UnsortedFormsMetadata,
  UnsortedChatMetadata,
  UnsortedSipMetadata,
  UnsortedMailMetadata,
  UnsortedTypes,
};
