/**
 * Интерфейс для работы с тегами
 * @see https://www.amocrm.ru/developers/content/crm_platform/tags-api
 * @version 4.0
 */
export interface TagBase {
  /**
   * Название тега
   * @type {string=}
   */
  name: string;

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;
}

export interface Tag extends TagBase {
  /**
   * ID тега
   * @type {number=}
   */
  id: number;

  /**
   * Название тега
   * @type {string=}
   */
  name: string;
}
