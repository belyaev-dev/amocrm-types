import {Entity, WithID} from './entity';
import {TagBase, Tag} from './tag';
import {Contact} from './contact';
import {CompanyBase, Company} from './company';
import {UnsortedFormsMetadata} from './unsorted';
import {
  BirthdayCustomField,
  CategoryCustomField,
  CheckboxCustomField,
  DateCustomField,
  ItemsCustomField,
  LegalEntityCustomField,
  MultiSelectCustomField,
  MultiTextCustomField,
  NumericCustomField,
  PriceCustomField,
  RadioButtonCustomField,
  SelectCustomField,
  StreetAddressCustomField,
  TextCustomField,
  TextareaCustomField,
  TrackingDataCustomField,
  UrlCustomField,
} from './custom_fields';
import {
  UnsortedChatMetadata,
  UnsortedMailMetadata,
  UnsortedSipMetadata,
} from '..';

/**
 * Интерфейс для работы со сделками
 * @see https://www.amocrm.ru/developers/content/crm_platform/leads-api
 * @version 4.0
 */
export interface NewLead extends Entity {
  /**
   * Название сделки (не обязательное)
   * @type {string=}
   */
  name?: string;

  /**
   * ID статуса (не обязательное)
   * @type {number=}
   */
  status_id?: number;

  /**
   * ID воронки (не обязательное)
   * @type {number=}
   */
  pipeline_id?: number;

  /**
   * Дата закрытия (timestamp) (не обязательное)
   * @type {number=}
   */
  closed_at?: number;

  /**
   * ID причины отказа (не обязательное)
   * @type {number=}
   */
  loss_reason_id?: number;

  /**
   * ID ответственного пользователя (не обязательное)
   * @type {number=}
   */
  responsible_user_id?: number;

  /**
   * ID источника (не обязательное)
   * @type {number=}
   */
  source_id?: number;

  /**
   * Visitor UID (не обязательное)
   * @type {string=}
   */
  visitor_uid?: string;

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;

  /**
   * Масси кастомных полей, если передается должнен быть хотябы 1 элемент
   * @type {CustomField}
   */
  custom_fields_values?: (
    | BirthdayCustomField
    | CategoryCustomField
    | CheckboxCustomField
    | DateCustomField
    | ItemsCustomField
    | LegalEntityCustomField
    | MultiSelectCustomField
    | MultiTextCustomField
    | NumericCustomField
    | PriceCustomField
    | RadioButtonCustomField
    | SelectCustomField
    | StreetAddressCustomField
    | TextCustomField
    | TextareaCustomField
    | TrackingDataCustomField
    | UrlCustomField
  )[];

  /**
   * Связанные сущности
   */
  _embedded?: {
    /**
     * Теги
     * @type {(TagBase | Tag)[]}
     */
    tags?: (TagBase | Tag)[];

    /**
     * Контакты (Только для метода complex)
     * @type {(ContactBase | Contact)[]}
     */
    contacts?: Contact[];

    /**
     * Компании (Только для метода complex)
     * @type {(CompanyBase | Company)[]}
     */
    companies?: (CompanyBase | Company)[];

    /**
     * Метаданнные для создания неразобранного
     * @type {(UnsortedChatMetadata | UnsortedSipMetadata | UnsortedFormsMetadata | UnsortedMailMetadata)}
     */
    metadata?:
      | UnsortedFormsMetadata
      | UnsortedSipMetadata
      | UnsortedChatMetadata
      | UnsortedMailMetadata;
  };
}

export interface CreatedLead extends NewLead {
  id: number;
  /**
   * Название сделки
   * @type {string=}
   */
  name: string;

  /**
   * ID статуса
   * @type {number}
   */
  status_id: number;

  /**
   * ID воронки
   * @type {number}
   */
  pipeline_id: number;

  /**
   * ID ответственного пользователя
   * @type {number}
   */
  responsible_user_id: number;
}

export type Lead = NewLead | CreatedLead;
