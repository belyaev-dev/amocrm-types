import {Entity, WithID} from './entity';
import {TagBase, Tag} from './tag';
import {
  BirthdayCustomField,
  CategoryCustomField,
  CheckboxCustomField,
  DateCustomField,
  ItemsCustomField,
  LegalEntityCustomField,
  MultiSelectCustomField,
  MultiTextCustomField,
  NumericCustomField,
  PriceCustomField,
  RadioButtonCustomField,
  SelectCustomField,
  StreetAddressCustomField,
  TextCustomField,
  TextareaCustomField,
  TrackingDataCustomField,
  UrlCustomField,
} from './custom_fields';

/**
 * Интерфейс для работы со компаниями
 * @see https://www.amocrm.ru/developers/content/crm_platform/companies-api
 * @version 4.0
 */
export interface CompanyBase extends Entity {
  /**
   * Название компании (не обязательное)
   * @type {string=}
   */
  name?: string;

  /**
   * ID ответственного пользователя (не обязательное)
   * @type {number=}
   */
  responsible_user_id?: number;

  /**
   * Связанные сущности
   */
  _embedded?: {
    /**
     * Теги
     * @type {Tag[]}
     */
    tags?: (TagBase | Tag)[];
  };

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;

  /**
   * Масси кастомных полей, если передается должнен быть хотябы 1 элемент
   * @type {CustomField}
   */
  custom_fields_values?: (
    | BirthdayCustomField
    | CategoryCustomField
    | CheckboxCustomField
    | DateCustomField
    | ItemsCustomField
    | LegalEntityCustomField
    | MultiSelectCustomField
    | MultiTextCustomField
    | NumericCustomField
    | PriceCustomField
    | RadioButtonCustomField
    | SelectCustomField
    | StreetAddressCustomField
    | TextCustomField
    | TextareaCustomField
    | TrackingDataCustomField
    | UrlCustomField
  )[];
}

export interface Company extends CompanyBase {

  id: number;
  /**
   * Название компании
   * @type {string=}
   */
  name: string;

  /**
   * ID ответственного пользователя
   * @type {number=}
   */
  responsible_user_id: number;
}
