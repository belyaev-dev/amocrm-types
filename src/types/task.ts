import {Entity, EntityTypes, WithID} from './entity';

export interface TaskResult {
  /**
   * Текст результата выполнения задачи
   * @type {string}
   */
  text: string;
}

/**
 * Интерфейс для работы с задачами
 * @see https://www.amocrm.ru/developers/content/crm_platform/tasks-api
 * @version 4.0
 */
export interface TaskBase extends Entity {
  /**
   * ID пользователя, ответственного за задачу
   * @type {number}
   */
  responsible_user_id?: number;

  /**
   * ID группы, в которой состоит ответственны пользователь за задачу
   * @type {number}
   */
  group_id?: number;

  /**
   * ID сущности, к которой привязана задача
   * @type {number}
   */
  entity_id: number;

  /**
   * Тип сущности, к которой привязана задача
   * @type {EntityTypes.Leads | EntityTypes.Contacts | EntityTypes.Companies}
   */
  entity_type: EntityTypes.Leads | EntityTypes.Contacts | EntityTypes.Companies;

  /**
   * Выполнена ли задача
   * @type {boolean}
   */
  is_completed?: boolean;

  /**
   * Тип задачи
   * @type {number}
   */
  task_type_id: number;

  /**
   * Описание задачи
   * @type {string}
   */
  text: string;

  /**
   * Длительность задачи в секундах
   * @type {number}
   */
  duration?: number;

  /**
   * Дата, когда задача должна быть завершена, передается в Unix Timestamp
   * @type {number}
   */
  complete_till: number;

  /**
   * Результат выполнения задачи
   * @type {TaskResult | null}
   */
  result?: TaskResult | null;
}

export interface Task extends TaskBase {
  /**
   * ID пользователя, ответственного за задачу
   * @type {number}
   */
  responsible_user_id: number;
}
