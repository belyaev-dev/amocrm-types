import {CustomField} from './custom_field';

export interface NumericCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Текстовое значение (число)
     * @type {string}
     */
    value: string;
  }[];
}
