import {CustomField} from './custom_field';

export interface CheckboxCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * true / false
     * @type {boolean}
     */
    value: boolean;
  }[];
}
