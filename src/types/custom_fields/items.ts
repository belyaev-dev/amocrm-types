import {CustomField} from './custom_field';

export enum ItemInitTypes {
  Piece = 'шт',
  Litre = 'л',
  // TODO Добавить все единицы измерения
}

export enum DiscountTypes {
  Percentage = 'percentage',
  Amount = 'amount',
}

export enum VatRates {
  NOT = 0,
  VAT_10 = 1,
  VAT_10_110 = 2,
  VAT_18 = 3,
  VAT_18_118 = 4,
  VAT_0 = 5,
}

export interface ItemDiscount {
  /**
   * Тип скидки
   * @type {DiscountTypes}
   */
  type: DiscountTypes;

  /**
   * Размер скидки
   * @type {number}
   */
  value: number;
}

export interface ItemsValue {
  /**
   * SKU товара (артикул)
   * @type {string}
   */
  sku?: string;

  /**
   * Описание товара
   * @type {string=}
   */
  description?: string;

  /**
   * Цена за единицу товара
   * @type {number=}
   */
  unit_price?: number;

  /**
   * Количество товара в счете
   * @type {number=}
   */
  quantity?: number;

  /**
   * Единица измерения
   * @type {ItemInitTypes=}
   */
  unit_type?: ItemInitTypes;

  /**
   * Объект скидки на товар
   * @type {ItemDiscount}
   */
  discount?: ItemDiscount;

  /**
   * Идентификатор налога
   * @type {VatRates}
   */
  vat_rate_id?: VatRates;

  /**
   * Идентификатор внешней системы
   * @type {string}
   */
  external_uid?: string;
}

export interface ItemsCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Значение
     * @type {ItemsValue}
     */
    value: ItemsValue;
  }[];
}
