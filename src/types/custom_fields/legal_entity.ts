import {CustomField} from './custom_field';

export enum LegalType {
  PERSON = 1,
  COMPANY = 2,
}

export interface LegalEntityValue {
  /**
   * Название организации
   * @type {string}
   */
  name: string;

  /**
   * Тип юридического лица
   * @type {LegalType}
   */
  entity_type: LegalType;

  /**
   * ИНН организации
   * @type {string=}
   */
  vat_id?: string;

  /**
   * ОГРНИП
   * @type {string=}
   */
  tax_registration_reason_code?: string;

  /**
   * Адрес организации
   * @type {string=}
   */
  address?: string;

  /**
   * КПП организации
   * @type {string=}
   */
  kpp?: string;

  /**
   * Идентификатор внешней системы
   * @type {string=}
   */
  external_uid?: string;
}

export interface LegalEntityCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Значение
     * @type {Legal}
     */
    value: LegalEntityValue;
  }[];
}
