import {CustomField} from './custom_field';

export const enum PhoneFieldEnumCodes {
  WORK,
  WORKDD,
  MOB,
  FAX,
  HOME,
  OTHER,
}

export const enum EmailFieldEnumCodes {
  WORK,
  PRIV,
  OTHER,
}

export interface MultiTextCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Текстовое значение
     * @type {string}
     */
    value: string;

    /**
     * ID значения поля
     * @type {number}
     */
    enum_id?: number;

    /**
     * Код значения поля
     * @type {EmailFieldEnumCodes|PhoneFieldEnumCodes}
     */
    enum_code?: EmailFieldEnumCodes | PhoneFieldEnumCodes;
  }[];
}
