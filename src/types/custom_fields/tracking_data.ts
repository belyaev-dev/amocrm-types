import {CustomField} from './custom_field';

export interface TrackingDataCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Текстовое значение
     * @type {string}
     */
    value: string;
  }[];
}
