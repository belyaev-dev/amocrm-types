import {CustomField} from './custom_field';

export interface CategoryCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Значение поля
     * @type {string}
     */
    value?: string;

    /**
     * ID значения поля (enum)
     * @type {number}
     */
    enum_id?: number;
  }[];
}
