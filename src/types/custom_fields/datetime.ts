import {CustomField} from './custom_field';

export interface DateTimeCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Значение поля – Unix Timestamp отметка или время в формате RFC 3339
     * (например, 2021-06-22T09:11:33+00:00)
     * @type {string|number}
     */
    value: string | number;
  }[];
}
