export enum DefaultCodes {
  PHONE = 'PHONE',
  EMAIL = 'EMAIL',
}

export interface CustomField {
  /**
   * ID поля (обязательное одно из двух полей)
   * @type {number}
   */
  field_id?: number;

  /**
   * CODE поля (обязательное одно из двух полей)
   * @type {number}
   */
  field_code?: string | DefaultCodes;
}
