import {CustomField} from './custom_field';

export interface TextareaCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Текстовое значение (многострочное)
     * @type {string}
     */
    value: string;
  }[];
}
