import {CustomField} from './custom_field';

export interface UrlCustomField extends CustomField {
  /**
   * В массиве всего 1 элемент Value
   * @type {Value[]}
   */
  values: {
    /**
     * Значение поля. Делегированный URL
     * @type {string}
     */
    value: string;
  }[];
}
