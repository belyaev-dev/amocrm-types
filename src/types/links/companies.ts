import {LinkTypes} from './types';
import {Link} from './link';

export interface CompaniesLink extends Link {
  /**
   * Тип связанной сущности
   * @type {LinkTypes}
   */
  to_entity_type: LinkTypes.Companies;

  /**
   * Метаданные
   * @type {null}
   */
  metadata?: null;
}
