import {LinkTypes} from './types';
import {Link} from './link';

export interface CatalogElementsLinkMetadata {
  /**
   * Количество прикрепленных элементов каталогов
   * @type {number}
   */
  quantity: number;

  /**
   * ID каталога
   * @type {number}
   */
  catalog_id: number;

  /**
   * ID поля типа Цена, которое установлено для привязанного элемента в контексте сущности
   * @type {number}
   */
  price_id?: number;
}

export interface CatalogElementsLink extends Link {
  /**
   * Тип связанной сущности
   * @type {LinkTypes}
   */
  to_entity_type: LinkTypes.CatalogElements;

  /**
   * Метаданные
   * @type {null}
   */
  metadata: CatalogElementsLinkMetadata;
}
