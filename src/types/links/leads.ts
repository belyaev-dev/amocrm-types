import {LinkTypes} from './types';
import {Link} from './link';

export interface LeadsLink extends Link {
  /**
   * Тип связанной сущности
   * @type {LinkTypes}
   */
  to_entity_type: LinkTypes.Leads;

  /**
   * Метаданные
   * @type {null}
   */
  metadata?: null;
}
