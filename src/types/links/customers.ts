import {LinkTypes} from './types';
import {Link} from './link';

export interface CustomersLink extends Link {
  /**
   * Тип связанной сущности
   * @type {LinkTypes}
   */
  to_entity_type: LinkTypes.Customers;

  /**
   * Метаданные
   * @type {null}
   */
  metadata?: null;
}
