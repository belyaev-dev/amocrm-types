import {LinkTypes} from './types';
import {Link} from './link';

export interface ContactLinkMetadata {
  /**
   * Является ли привязанный контакт главным
   * @type {boolean}
   */
  main_contact: boolean;
}

export interface ContactsLink extends Link {
  /**
   * Тип связанной сущности
   * @type {LinkTypes}
   */
  to_entity_type: LinkTypes.Contacts;

  /**
   * Метаданные
   * @type {null}
   */
  metadata: ContactLinkMetadata;
}
