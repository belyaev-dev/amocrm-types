export enum LinkTypes {
  Leads = 'leads',
  Contacts = 'contacts',
  Companies = 'companies',
  Customers = 'customers',
  CatalogElements = 'catalog_elements',
}
