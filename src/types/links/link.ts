import {LinkTypes} from './types';

export interface Link {
  /**
   * ID связанной сущности
   * @type {number}
   */
  to_entity_id: number;

  /**
   * Тип связанной сущности
   * @type {LinkTypes}
   */
  to_entity_type: LinkTypes;
}
