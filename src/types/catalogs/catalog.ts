import {Entity} from '../entity';
import {Tag} from '../tag';
import {
  BirthdayCustomField,
  CategoryCustomField,
  CheckboxCustomField,
  DateCustomField,
  ItemsCustomField,
  LegalEntityCustomField,
  MultiSelectCustomField,
  MultiTextCustomField,
  NumericCustomField,
  PriceCustomField,
  RadioButtonCustomField,
  SelectCustomField,
  StreetAddressCustomField,
  TextCustomField,
  TextareaCustomField,
  TrackingDataCustomField,
  UrlCustomField,
} from '../custom_fields';

export enum CatalogTypes {
  Regular = 'regular',
  Invoices = 'invoices',
  Products = 'products',
}

export interface Catalog extends Entity {
  /**
   * ID (не обязательное)
   * @type {number=}
   */
  id?: number;

  /**
   * Название  (не обязательное)
   * @type {string=}
   */
  name?: string;

  /**
   * Сортировка списка (не обязательное)
   * @type {number=}
   */
  sort?: number;

  /**
   * Если ли возможность привязывать один элемент данного списка к нескольким сделкам/покупателям
   * @type {boolean=}
   */
  can_link_multiple?: boolean;

  /**
   * Может ли список быть удален через интерфейс
   * @type {boolean=}
   */
  can_be_deleted?: boolean;

  /**
   * ID аккаунта, в котором находится список
   * @type {number}
   */
  account_id?: number;

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;
}

export interface CatalogElementBase extends Entity {
  /**
   * ID списка где распологается элемент
   * @type {number}
   */
  catalog_id: number;

  /**
   * Название (не обязательное)
   * @type {string=}
   */
  name: string;

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;

  /**
   * Массив кастомных полей, если передается должнен быть хотябы 1 элемент
   * @type {CustomField}
   */
  custom_fields_values?: (
    | BirthdayCustomField
    | CategoryCustomField
    | CheckboxCustomField
    | DateCustomField
    | ItemsCustomField
    | LegalEntityCustomField
    | MultiSelectCustomField
    | MultiTextCustomField
    | NumericCustomField
    | PriceCustomField
    | RadioButtonCustomField
    | SelectCustomField
    | StreetAddressCustomField
    | TextCustomField
    | TextareaCustomField
    | TrackingDataCustomField
    | UrlCustomField
  )[];
}

export interface CatalogElement extends CatalogElementBase {
  /**
   * ID аккаунта, в котором находится элемент
   * @type {number}
   */
  account_id: number;

  /**
   * Удален ли элемент
   * @type {boolean}
   */
  is_deleted?: boolean | null;

  /**
   * Cсылка на печатную форму счета
   * @type {string}
   */
  invoice_link?: string | null;
}
