import {Catalog, CatalogTypes, CatalogElement} from './catalog';
import {WithID, Entity} from '../entity';
import {
  BirthdayCustomField,
  CategoryCustomField,
  CheckboxCustomField,
  DateCustomField,
  ItemsCustomField,
  LegalEntityCustomField,
  MultiSelectCustomField,
  MultiTextCustomField,
  NumericCustomField,
  PriceCustomField,
  RadioButtonCustomField,
  SelectCustomField,
  StreetAddressCustomField,
  TextCustomField,
  TextareaCustomField,
  TrackingDataCustomField,
  UrlCustomField,
} from '../custom_fields';

export enum InvoiceCustomFieldsCodes {
  BILL_STATUS,
  LEGAL_ENTITY,
  PAYER,
  BILL_VAT_TYPE,
  BILL_PAYMENT_DATE,
  ITEMS,
  BILL_COMMENT,
  BILL_PRICE,
  RECEIPT_LINK,
  EXTERNAL_ID,
}

export interface InvoiceCatalog extends Catalog {
  /**
   * Тип списка
   * @type {CatalogTypes}
   */
  type: CatalogTypes.Invoices;

  /**
   * Можно ли добавлять элементы списка из интерфейса
   * (Применяется только для списка счетов)
   * @type {boolean=}
   */
  can_add_elements: boolean;

  /**
   * Должна ли добавляться вкладка со списком в карточку сделки/покупателя
   * (Применяется только для списка счетов)
   * @type {boolean=}
   */
  can_show_in_cards: boolean;

  /**
   * Код виджета, который управляет списком и может отобразить своё собственное окно редактирования элемента
   * (Применяется только для списка счетов)
   * @type {boolean=}
   */
  sdk_widget_code: number | null;
}

export interface InvoiceBase extends Entity {
  /**
   * ID списка где распологается элемент
   * @type {number}
   */
  catalog_id: number;

  /**
   * Название (не обязательное)
   * @type {string=}
   */
  name: string;

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;

  /**
   * Массив кастомных полей, если передается должнен быть хотябы 1 элемент
   * @type {CustomField}
   */
  custom_fields_values?: (
    | CategoryCustomField
    | CheckboxCustomField
    | DateCustomField
    | ItemsCustomField
    | LegalEntityCustomField
    | MultiSelectCustomField
    | MultiTextCustomField
    | NumericCustomField
    | PriceCustomField
    | RadioButtonCustomField
    | SelectCustomField
    | StreetAddressCustomField
    | TextCustomField
    | TextareaCustomField
    | TrackingDataCustomField
    | UrlCustomField
  )[];
}

export interface Invoice extends InvoiceBase {

  id: number;
  /**
   * ID аккаунта, в котором находится элемент
   * @type {number}
   */
  account_id: number;

  /**
   * Удален ли элемент
   * @type {boolean}
   */
  is_deleted?: boolean | null;

  /**
   * Cсылка на печатную форму счета
   * @type {string}
   */
  invoice_link?: string | null;
}
