import {Entity} from './entity';
import {Tag} from './tag';
import {
  BirthdayCustomField,
  CategoryCustomField,
  CheckboxCustomField,
  DateCustomField,
  ItemsCustomField,
  LegalEntityCustomField,
  MultiSelectCustomField,
  MultiTextCustomField,
  NumericCustomField,
  PriceCustomField,
  RadioButtonCustomField,
  SelectCustomField,
  StreetAddressCustomField,
  TextCustomField,
  TextareaCustomField,
  TrackingDataCustomField,
  UrlCustomField,
} from './custom_fields';


export enum CatalogTypes {
	Regular  = 'regular',
	Invoices = 'invoices',
	Products = 'products',
}

export interface Catalog extends Entity {
  /**
   * ID (не обязательное)
   * @type {number=}
   */
  id?: number;

  /**
   * Название  (не обязательное)
   * @type {string=}
   */
  name?: string;

  /**
   * Сортировка списка (не обязательное)
   * @type {number=}
   */
  sort?: number;

  /**
   * Тип списка
   * @type {CatalogTypes}
   */
  type: CatalogTypes,

  /**
   * Можно ли добавлять элементы списка из интерфейса 
   * (Применяется только для списка счетов)
   * @type {boolean=}
   */
  can_add_elements?: boolean;

  /**
   * Должна ли добавляться вкладка со списком в карточку сделки/покупателя 
   * (Применяется только для списка счетов)
   * @type {boolean=}
   */
  can_show_in_cards?: boolean;

  /**
   * Если ли возможность привязывать один элемент данного списка к нескольким сделкам/покупателям
   * @type {boolean=}
   */
  can_link_multiple?: boolean;

   /**
   * Может ли список быть удален через интерфейс
   * @type {boolean=}
   */
  can_be_deleted?: boolean;

  /**
   * Код виджета, который управляет списком и может отобразить своё собственное окно редактирования элемента 
   * (Применяется только для списка счетов)
   * @type {boolean=}
   */
  sdk_widget_code?: number | null;

  /**
   * ID аккаунта, в котором находится список
   * @type {number}
   */
  account_id?: number;

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;


}


export interface CatalogElement extends Entity {

   /**
   * ID (не обязательное)
   * @type {number=}
   */
  id?: number;

  /**
   * ID списка где распологается элемент
   * @type {number}
   */
  catalog_id?: number;

  /**
   * Название (не обязательное)
   * @type {string=}
   */
  name: string;

  /**
   * Удален ли элемент
   * @type {boolean}
   */
  is_deleted?: boolean | null;

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;

  /**
   * Cсылка на печатную форму счета
   * @type {string}
   */
  invoice_link?: string | null;

  /**
   * Массив кастомных полей, если передается должнен быть хотябы 1 элемент
   * @type {CustomField}
   */
  custom_fields_values?: (
    | BirthdayCustomField
    | CategoryCustomField
    | CheckboxCustomField
    | DateCustomField
    | ItemsCustomField
    | LegalEntityCustomField
    | MultiSelectCustomField
    | MultiTextCustomField
    | NumericCustomField
    | PriceCustomField
    | RadioButtonCustomField
    | SelectCustomField
    | StreetAddressCustomField
    | TextCustomField
    | TextareaCustomField
    | TrackingDataCustomField
    | UrlCustomField
  )[];

   /**
   * ID аккаунта, в котором находится элемент
   * @type {number}
   */
  account_id?: number;

}
