import {Entity} from './entity';
import {TagBase, Tag} from './tag';
import {
  BirthdayCustomField,
  CategoryCustomField,
  CheckboxCustomField,
  DateCustomField,
  ItemsCustomField,
  LegalEntityCustomField,
  MultiSelectCustomField,
  MultiTextCustomField,
  NumericCustomField,
  PriceCustomField,
  RadioButtonCustomField,
  SelectCustomField,
  StreetAddressCustomField,
  TextCustomField,
  TextareaCustomField,
  TrackingDataCustomField,
  UrlCustomField,
} from './custom_fields';

/**
 * Интерфейс для работы с контактами
 * @see https://www.amocrm.ru/developers/content/crm_platform/contacts-api
 * @version 4.0
 */
export interface NewContact extends Entity {
  /**
   * Название контакта (не обязательное)
   * @type {string=}
   */
  name?: string;

  /**
   * Имя контакта (не обязательное)
   * @type {string=}
   */
  first_name?: string;

  /**
   * Фамилия контакта (не обязательное)
   * @type {string=}
   */
  last_name?: string;

  /**
   * ID ответственного пользователя (не обязательное)
   * @type {number=}
   */
  responsible_user_id?: number;

  /**
   * Связанные сущности
   */
  _embedded?: {
    /**
     * Теги
     * @type {Tag[]}
     */
    tags?: (TagBase | Tag)[];
  };

  /**
   * Вернется в ответе без изменений (не обязательное)
   * @type {string=}
   */
  request_id?: string;

  /**
   * Масси кастомных полей, если передается должнен быть хотябы 1 элемент
   * @type {CustomField}
   */
  custom_fields_values?: (
    | BirthdayCustomField
    | CategoryCustomField
    | CheckboxCustomField
    | DateCustomField
    | ItemsCustomField
    | LegalEntityCustomField
    | MultiSelectCustomField
    | MultiTextCustomField
    | NumericCustomField
    | PriceCustomField
    | RadioButtonCustomField
    | SelectCustomField
    | StreetAddressCustomField
    | TextCustomField
    | TextareaCustomField
    | TrackingDataCustomField
    | UrlCustomField
  )[];
}

export interface CreatedContact extends NewContact {

  id: number;
  /**
   * Название контакта (не обязательное)
   * @type {string=}
   */
  name: string;

  /**
   * ID ответственного пользователя (не обязательное)
   * @type {number=}
   */
  responsible_user_id: number;
}


export type Contact = CreatedContact | NewContact;


export interface ContactQuery {
  with?: string;
  page?: number;
  limit?: number;
  query?: string;
  order?: {
    updated_at?: string;
    id: string;
  }
}