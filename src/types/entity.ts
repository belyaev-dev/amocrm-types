export enum EntityTypes {
  Leads = 'leads',
  Contacts = 'contacts',
  Companies = 'companies',
  Customers = 'customers',
  Catalogs = 'catalogs',
  CatalogElements = 'catalog_elements',
}

/**
 * Базовая сущность amoCRM
 */
export interface Entity {
  /**
   * ID пользователя создавшего сущность (не обязательное)
   * @type {number=}
   * @example 0 = Robot
   */
  created_by?: number;

  /**
   * ID пользователя обновившего сущность (не обязательное)
   * @type {number=}
   * @example 0 = Robot
   */
  updated_by?: number;

  /**
   * Дата создания (timestamp) (не обязательное)
   * @type {number=}
   */
  created_at?: number;

  /**
   * Дата обновления (timestamp) (не обязательное)
   * @type {number=}
   */
  updated_at?: number;

  _links?: {
    self: {
      href: string;
    };
  };
}

/**
 * Созданная сущность с ID
 */
export interface WithID extends Entity {
  /**
   * ID Сущности
   * @type {number}
   */
  id: number;

  /**
   * ID пользователя создавшего сущность
   * @type {number}
   * @example 0 = Robot
   */
  created_by: number;

  /**
   * ID пользователя обновившего сущность
   * @type {number}
   * @example 0 = Robot
   */
  updated_by: number;

  /**
   * Дата создания (timestamp)
   * @type {number}
   */
  created_at: number;

  /**
   * Дата обновления (timestamp)
   * @type {number}
   */
  updated_at: number;
}


export type Base = WithID | Entity;
