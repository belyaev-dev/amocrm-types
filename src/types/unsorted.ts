export const enum UnsortedTypes {
  SIP = 'sip',
  FORMS = 'forms',
  CHAT = 'chat',
  MAIL = 'mail',
}

export interface UnsortedFormsMetadata {
  /**
   * Тип неразобранного (forms)
   * @type {UnsortedTypes.FORMS}
   */
  category: UnsortedTypes.FORMS;

  /**
   * Идентификатор формы на стороне интеграции
   * @type {string}
   */
  form_id: string;

  /**
   * Название формы
   * @type {string}
   */
  form_name: string;

  /**
   * Страница на который установлена форма
   * @type {string}
   */
  form_page: string;

  /**
   * IP адрес с которого поступила заявка
   * @type {string}
   */
  ip: string;

  /**
   * Временная метка отправки данных через форму. Данные в формате Unix Timestamp
   * @type {number}
   */
  form_sent_at: number;

  /**
   * Информация, откуда был переход на страницу, где расположена форма
   * @type {string}
   */
  referer: string;
}

export interface UnsortedSipMetadata {
  /**
   * Тип неразобранного (sip)
   * @type {UnsortedTypes.SIP}
   */
  category: UnsortedTypes.SIP;

  /**
   * От кого сделан звонок
   * @type {string}
   */
  from: string;

  /**
   * Кому сделан звонок
   * @type {string}
   */
  phone: string;

  /**
   * Когда сделан звонок в формате Unix Timestamp.
   * @type {number}
   */
  called_at: number;

  /**
   * Сколько длился звонок
   * @type {number}
   */
  duration: number;

  /**
   * Ссылка на запись звонка
   * @type {string}
   */
  link: string;

  /**
   * Код сервиса, через который сделан звонок
   * @type {string}
   */
  service_code: string;

  /**
   * В случае передачи значения true, в карточку будет добавлено событие о входящем звонке.
   * Данный флаг не возвращается в API, но может быть передан.
   * @type {boolean=}
   */
  is_call_event_needed: boolean;
}

export interface UnsortedChatOrigin {
  /**
   * ID чата в сервисе чатов
   * @type {string}
   */
  chat_id: string;
  /**
   * Дополнительные данные из сервиса чатов
   * @type {string=}
   */
  ref?: string;
  /**
   * UID пользователя, который написал сообщение
   * @type {string=}
   */
  visitor_uid?: string;
}
export interface UnsortedChatClient {
  /**
   * Имя отправителя сообщения
   * @type {string}
   */
  name: string;
  /**
   * Ссылка на аватар отправителя сообщения
   * @type {string=}
   */
  avatar?: string;
}
export interface UnsortedChatMetadata {
  /**
   * Тип неразобранного (chat)
   * @type {UnsortedTypes.CHAT}
   */
  category: UnsortedTypes.CHAT;

  /**
   * Имя пользователя, который написал в чат
   * @type {string}
   */
  from: string;

  /**
   * Идентификатор страницы, на которую написано сообщение
   * @type {string}
   */
  to: string;

  /**
   * Когда получено сообщение от клиента. Данные в формате Unix Timestamp.
   * @type {number}
   */
  recieved_at: number;

  /**
   * Название сервиса, из которого получено сообщение
   * @type {string}
   */
  service: string;

  /**
   * Данные об авторе сообщения
   * @type {UnsortedChatClient}
   */
  client: UnsortedChatClient;

  /**
   * Информация, откуда был переход на страницу, где расположена форма
   * @type {UnsortedChatOrigin}
   */
  origin: UnsortedChatOrigin;

  /**
   * Последнее сообщение от пользователя
   * @type {string}
   */
  last_message_text?: string;

  /**
   * Название источника, из которого получено сообщение
   * @type {string}
   */
  source_name: string;
}

export interface UnsortedMailOrigin {
  /**
   * E-mail автора письма
   * @type {string}
   */
  email: string;
  /**
   * Имя автора письма
   * @type {string}
   */
  name: string;
}
export interface UnsortedMailMetadata {
  /**
   * Тип неразобранного (mail)
   * @type {UnsortedTypes.MAIL}
   */
  category: UnsortedTypes.MAIL;

  /**
   * Информация об авторе письма
   * @type {UnsortedMailOrigin}
   */
  from: UnsortedMailOrigin;

  /**
   * Когда получено письмо. Данные в формате Unix Timestamp.
   * @type {number}
   */
  recieved_at: number;

  /**
   * Тема письма
   * @type {string}
   */
  subject: string;

  /**
   * ID цепочки писем в сервисе почты
   * @type {number}
   */
  thread_id: number;

  /**
   * ID письма в сервисе почты
   * @type {number}
   */
  message_id: number;

  /**
   * Выдержка из контента письма
   * @type {string}
   */
  content_summary: string;
}
